<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viajes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo',10)->unique();
            $table->string('n_plazas',50);
			$table->string('destino',256);
			$table->string('origen',15);
			$table->decimal('precio',10,2);
            $table->timestamps();
        });

        //Viajeros & Viajes => viajero & viaje => viajero_viaje
        Schema::create('viajero_viaje', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('viajero_id')->unsigned();
            $table->integer('viaje_id')->unsigned();

            $table->foreign('viajero_id')->references('id')->on('viajeros');
            $table->foreign('viaje_id')->references('id')->on('viajes');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('viajes');
    }
}
