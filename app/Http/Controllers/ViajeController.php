<?php

namespace App\Http\Controllers;

use App\Viaje;
use Illuminate\Http\Request;

class ViajeController extends Controller
{
    
    /**
     * Muestra la lista de Viajes registrados.
     *
     * @returna \Illuminate\Http\Response
     */
    public function index()
    {
        $viaje = Viaje::all();
        return response()->json(['viajes'=>$viaje]);
    } 


    /** Guarda un viaje nuevo
     * Parametros: Datos del viaje
     * @retorna \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if($request)
        {
        	$viaje = new Viaje($request->all());
	    	$viaje->save();    
            return response()->json(['message' => 'Viaje Guardado']);
        }
    }
 
    /**
     * Muestra un Viaje especificado.
     *
     * parametro  int  $id
     * retorna \Illuminate\Http\Response
     */
    public function show($id)
    {
        $viaje = Viaje::find($id);
        return response()->json(['viaje' => $viaje]);
    }
 
    /**
     * Actualiza los datos de un viaje.
     *
     * parametros  \Illuminate\Http\Request  $request
     * parametros  int  $id
     * retorno \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request)
        {
            $viaje = Viaje::find($id);
            $viaje->codigo = $request->input('codigo');
            $viaje->n_plazas = $request->input('n_plazas');
            $viaje->destino = $request->input('destino');
            $viaje->origen = $request->input('origen');
            $viaje->precio = $request->input('precio');
            $viaje->save();
            return response()->json(['message' => 'Viaje Actualizado']);
        }
    }
 
    /**
     * Elimina el viaje con el id especifico.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $viaje = Viaje::find($id);
        $viaje->delete();
        return response()->json(['message' => 'Viaje Eliminado']);
    }


}
