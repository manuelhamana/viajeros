<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Viaje extends Model
{
    protected $table = "viajes";

    protected $fillable =['codigo','n_plazas','destino','origen', 'precio'];

    public function viajeros(){
    	return $this->belongsToMany('App\Viajero','viajero_viaje');
    }
}
